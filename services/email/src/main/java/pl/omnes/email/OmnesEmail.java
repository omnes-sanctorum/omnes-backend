package pl.omnes.email;

import org.springframework.boot.SpringApplication;
import pl.omnes.core.annotations.OmnesApplication;

/**
 * Main class for running Omnes Email Service
 */
@OmnesApplication
public class OmnesEmail {
    public static void main(String[] args) {
        SpringApplication.run(OmnesEmail.class, args);
    }
}

package pl.omnes.scheduler;

import org.springframework.boot.SpringApplication;
import pl.omnes.core.annotations.OmnesApplication;

/**
 * Main class for running Omnes Scheduler Service
 */
@OmnesApplication
public class OmnesScheduler {
    public static void main(String[] args) {
        SpringApplication.run(OmnesScheduler.class, args);
    }
}

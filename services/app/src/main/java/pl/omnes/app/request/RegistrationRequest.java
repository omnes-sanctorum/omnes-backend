package pl.omnes.app.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.omnes.core.model.ParishFunction;

/**
 * Request class for user registration
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationRequest {

    private String username;

    private String firstName;

    private String lastName;

    //TODO: Add password pattern
    private String password;

    //TODO: Add email pattern
    private String email;

    private ParishFunction parishFunction;

    private String city;

    private String province;
}

package pl.omnes.app.service.impl;

import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.omnes.app.request.RegistrationRequest;
import pl.omnes.app.service.RegistrationService;
import pl.omnes.core.model.User;
import pl.omnes.core.repository.UserRepository;

import static pl.omnes.core.model.UserStatus.CREATED;

/**
 * Implementation of {@link RegistrationService}
 */
@Service
public class RegistrationServiceImpl implements RegistrationService {

    private static final Logger LOG = LoggerFactory.getLogger(RegistrationServiceImpl.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    /**
     * Constructor injecting all required dependencies
     *
     * @param userRepository  {@link UserRepository}
     * @param passwordEncoder {@link PasswordEncoder}
     */
    @Autowired
    public RegistrationServiceImpl(final UserRepository userRepository,
                                   final PasswordEncoder passwordEncoder) {
        LOG.debug("Initializing Registration Service");
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void registerUser(@NonNull final RegistrationRequest registrationRequest) {
        LOG.debug("Sending user registration request");

        //TODO: Throw BadRequestException instead of ending method execution
        LOG.debug("Checking if user with provided email already exists");
        if (userRepository.findByEmail(registrationRequest.getEmail()).isPresent()) {
            return;
        }

        final User user = User.builder()
                .username(registrationRequest.getUsername())
                .firstName(registrationRequest.getFirstName())
                .lastName(registrationRequest.getLastName())
                .password(passwordEncoder.encode(registrationRequest.getPassword()))
                .email(registrationRequest.getEmail())
                .parishFunction(registrationRequest.getParishFunction())
                .city(registrationRequest.getCity())
                .province(registrationRequest.getProvince())
                .status(CREATED)
                .build();

        LOG.debug("Saving user");
        userRepository.save(user);
    }
}

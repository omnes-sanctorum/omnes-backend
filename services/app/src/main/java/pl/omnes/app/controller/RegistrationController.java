package pl.omnes.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.omnes.app.request.RegistrationRequest;
import pl.omnes.app.service.RegistrationService;

import javax.validation.Valid;

/**
 * REST Controller for users registration
 */
@RestController
@RequestMapping("/user")
public class RegistrationController {

    private static final Logger LOG = LoggerFactory.getLogger(RegistrationController.class);

    private final RegistrationService registrationService;

    /**
     * Constructor injecting all required dependencies
     *
     * @param registrationService {@link RegistrationService}
     */
    @Autowired
    public RegistrationController(final RegistrationService registrationService) {
        LOG.debug("Initializing Registration Controller");
        this.registrationService = registrationService;
    }

    @PostMapping
    public void registerUser(@Valid @RequestBody final RegistrationRequest registrationRequest) {
        registrationService.registerUser(registrationRequest);
    }
}

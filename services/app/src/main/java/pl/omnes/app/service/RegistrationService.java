package pl.omnes.app.service;

import pl.omnes.app.request.RegistrationRequest;

/**
 * Service interface for users registration
 */
public interface RegistrationService {

    /**
     * Method registers user
     *
     * @param registrationRequest {@link RegistrationRequest}
     */
    void registerUser(RegistrationRequest registrationRequest);
}

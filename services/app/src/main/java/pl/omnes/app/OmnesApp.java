package pl.omnes.app;

import org.springframework.boot.SpringApplication;
import pl.omnes.core.annotations.OmnesApplication;

/**
 * Main class for running Omnes App Service
 */
@OmnesApplication
public class OmnesApp {
    public static void main(String[] args) {
        SpringApplication.run(OmnesApp.class, args);
    }
}

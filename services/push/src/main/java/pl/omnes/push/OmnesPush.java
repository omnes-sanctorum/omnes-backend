package pl.omnes.push;

import org.springframework.boot.SpringApplication;
import pl.omnes.core.annotations.OmnesApplication;

/**
 * Main class for running Omnes Push Service
 */
@OmnesApplication
public class OmnesPush {
    public static void main(String[] args) {
        SpringApplication.run(OmnesPush.class, args);
    }
}

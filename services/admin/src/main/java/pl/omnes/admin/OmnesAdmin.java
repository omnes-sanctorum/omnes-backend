package pl.omnes.admin;

import org.springframework.boot.SpringApplication;
import pl.omnes.core.annotations.OmnesApplication;

/**
 * Main class for running Omnes Admin Service
 */
@OmnesApplication
public class OmnesAdmin {
    public static void main(String[] args) {
        SpringApplication.run(OmnesAdmin.class, args);
    }
}

package pl.omnes.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.omnes.core.model.User;

import java.util.Optional;

/**
 * Repository for users management
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Method returns user by email
     *
     * @param email user email
     * @return {@link Optional} of {@link User}
     */
    Optional<User> findByEmail(String email);
}

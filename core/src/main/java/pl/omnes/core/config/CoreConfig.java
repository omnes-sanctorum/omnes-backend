package pl.omnes.core.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Core module configuration class
 */
@Configuration
@EnableJpaRepositories(basePackages = {"pl.omnes.core"})
@EntityScan(basePackages = {"pl.omnes.core"})
@ComponentScan(basePackages = {"pl.omnes.core"})
public class CoreConfig {
}

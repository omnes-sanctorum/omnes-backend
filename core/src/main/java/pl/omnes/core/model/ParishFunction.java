package pl.omnes.core.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enum class describing parish function
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ParishFunction {

    PARISHIONER("PARISHIONER");

    private String name;
}

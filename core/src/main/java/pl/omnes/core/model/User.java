package pl.omnes.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;

/**
 * Model class for user entity
 */
@Table(name = "users")
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;

    @NotNull
    @Size(min = 2, max = 255)
    @Column(nullable = false, unique = true)
    private String username;

    @NotNull
    @Size(min = 2, max = 255)
    @Column(nullable = false)
    private String firstName;

    @NotNull
    @Size(min = 2, max = 255)
    @Column(nullable = false)
    private String lastName;

    @NotNull
    @JsonIgnore
    @Column(nullable = false)
    private String password;

    @NotNull
    @Email
    @Column(nullable = false, unique = true)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private UserStatus status;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ParishFunction parishFunction;

    @NotNull
    @Column
    private String city;

    @NotNull
    @Column
    private String province;

    @CreationTimestamp
    @Column(updatable = false)
    private OffsetDateTime creationTimestamp;

    @UpdateTimestamp
    @Column
    private OffsetDateTime updateTimestamp;
}

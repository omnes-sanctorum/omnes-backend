package pl.omnes.core.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enum class describing user status
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum UserStatus {

    CREATED("CREATED"),
    ACTIVE("ACTIVE"),
    BLOCKED("BLOCKED"),
    RESTRICTED("RESTRICTED");

    private String name;
}
